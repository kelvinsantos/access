<?php
session_start();
?>

<?php
	// CSS
	$jquerydatatablescss = "1";
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$jquerydatatablesjs = "1";
?>

<?php 
	$title = "A.C.C.E.S.S. - Search Results";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/publicHeader.php");
?>

<script type="text/javascript">
	$(document).ready(function() {

	    // Setup - add a text input to each footer cell
	    $('#bookArchiveTable tfoot tr th').each(function() {
	        var title = $('#bookArchiveTable thead tr th').eq($(this).index()).text();
	        $(this).html('<input type="text" placeholder="Search '+title+'" />');
	    });
	 
	    // DataTable
	    var table = $('#bookArchiveTable').DataTable();
	 
	    // Apply the search
	    table.columns().eq(0).each(function(colIdx) {
	        $('input', table.column(colIdx).footer()).on('keyup change', function() {
	            table
	                .column(colIdx)
	                .search(this.value)
	                .draw();
	        });
	    });

		$('#bookArchiveTable #idx').on('click', function() {
			var bookId = $(this).attr('bookId');
			window.open("/access/home/basic.php?bookId=" + bookId, '_blank');
		});
	
	});
</script>

<?php

$query = $_POST['query'];

$query = stripslashes($query);

$result = mysqli_query($mysqli,"SELECT * FROM book_information WHERE book_title LIKE '%".$query."%' OR book_school LIKE '%".$query."%' OR book_department LIKE '%".$query."%' OR book_course LIKE '%".$query."%' OR book_date_finished LIKE '%".$query."%'");
?>
<div class='container'>
<div class="row">
  <div class="col-md-11">
  
	<h3><a href="../public_accounts/return.php" class="label label-primary">< Return to Search</a></h3>
	<h3><span class="label label-primary">Search Results for: <?php echo $query;?></span></h3>
  </div>
</div>
<br />
	<table cellpadding='0' cellspacing='0' border='0' class='display' id='bookArchiveTable'>
        <thead>
            <tr>
                <th>Title</th>
                <th>School</th>
                <th>Department</th>
                <th>Course</th>
                <th>Date Finished</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>Title</th>
                <th>School</th>
                <th>Department</th>
                <th>Course</th>
                <th>Date Finished</th>
            </tr>
        </tfoot>
    <tbody>

		<?php
		while($row = mysqli_fetch_array($result))
		  {
		  echo "<tr id='idx' bookId=".$row['book_id'].">";
		  echo "<td>".$row['book_title']."</td>";
		  echo "<td>".$row['book_school']."</td>";
		  echo "<td>".$row['book_department']."</td>";
		  echo "<td>".$row['book_course']."</td>";
		  echo "<td>".$row['book_date_finished']."</td>";
		  echo "</tr>";
		  }
		  
		  mysqli_close($mysqli);
		?>

	</tbody>
	</table>
	<h3><span class="label label-primary">Click a result to view</span></h3>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>