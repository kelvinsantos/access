<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Administrator List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	if (isset($_GET['op'])) {
		if ($_GET['op'] == 'deleteAdmin') {
		
			$sql="UPDATE accounts SET is_deleted = '1' WHERE account_id = '".$_GET['accountId']."'";
		
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			mysqli_close($mysqli);
							
			echo "<script>
			alert('Delete Success!');
			window.location.href = 'adminInformationList.php';
			</script>";

			break;
		}
	}
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#addAdmin").click(function() {
		window.location.href = "adminInformationMaintenance.php";	
	});
});

</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<h3><span class="label label-primary"><?php echo $title ?></span></h3>
<br />
<div class="table-responsive">
	<table class="table table-striped">
  		<tr>
        	<th>#</th>
            <th>Full Name</th>
			<th>Position</th>
            <th>Date of Birth</th>
            <th>Action</th>

        </tr>
        
        <?php 
		$result = mysqli_query($mysqli,"SELECT * FROM accounts AS a INNER JOIN admin_information AS b ON a.account_id = b.account_id WHERE a.role = 'admin' AND a.is_deleted = '0'");
		while ($row = mysqli_fetch_array($result)) {
		?>
        
        <tr>
        	<td><?php echo $row['admin_id']; ?></td>
            <td><?php echo $row['full_name']; ?></td>
            <td><?php echo $row['position']; ?></td>
            <td><?php echo $row['date_of_birth']; ?></td>
            <td><a href="adminInformationMaintenance.php?accountId=<?php echo $row['account_id']; ?>">Edit</a> | <a href="adminInformationList.php?op=deleteAdmin&accountId=<?php echo $row['account_id']; ?>">Delete</a></td>
        </tr>
        
        <?php } ?>
	</table>
    <button type="button" class="btn btn-primary pull-right" name="addAdmin" id="addAdmin">Add Admin</button>
</div>
</form>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>