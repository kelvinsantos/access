<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$jqueryuicss = "1";
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	$bootstrapdatepickercss = "1";
	
	// JS
	$jqueryjs = "1";
	$jqueryuijs = "1";
	$bootstrapjs = "1";
	$bootstrapdatepicker = "1";

	$title = "Book List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	if (isset($_POST['save'])) {
		$bookId = $_POST['bookId'];
		if ($bookId != null && $bookId != "0") {
			
			$sql = "UPDATE book_information SET
				book_title = '".$_POST['bookTitle']."',
				book_school = '".$_POST['bSchool']."',
				book_department = '".$_POST['bDepartment']."',
				book_course = '".$_POST['bCourse']."',
				book_date_finished = '".$_POST['bookDateFinished']."',
				book_author_1 = '".$_POST['bookAuthor1']."',
				book_author_2 = '".$_POST['bookAuthor2']."',
				book_author_3 = '".$_POST['bookAuthor3']."',
				book_author_4 = '".$_POST['bookAuthor4']."'
				WHERE book_id = '".$_POST['bookId']."'";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
					
				mysqli_close($mysqli);
				
				echo "<script>
				alert('Update Success!');
				window.location.href = 'bookInformationMaintenance.php?bookId=".$_POST['bookId']."';
				</script>";
				
		} else {
				
			$sql = "SELECT book_title FROM book_information WHERE book_title = '".$_POST['bookTitle']."'";
			$result = $mysqli->query($sql);

			if ($result->num_rows > 0) {
			    echo "<script>
				alert('Book title already exists!');
				window.location.href = 'bookInformationMaintenance.php';
				</script>";
			} else {
			    
			    $sql = "INSERT INTO book_information (book_id, book_title, book_school, book_department, book_course, book_date_finished, book_author_1,
				book_author_2, book_author_3, book_author_4) 
				VALUES 
				('".$bookId."', '".$_POST['bookTitle']."', '".$_POST['bSchool']."','".$_POST['bDepartment']."', '".$_POST['bCourse']."', '".$_POST['bookDateFinished']."', 
				'".$_POST['bookAuthor1']."', '".$_POST['bookAuthor2']."', '".$_POST['bookAuthor3']."', '".$_POST['bookAuthor4']."')";
				
				if (!mysqli_query($mysqli,$sql)) {
					die('Error: ' . mysqli_error($mysqli));
				}
			
				mysqli_close($mysqli);
				
				echo "<script>
				alert('Insert Success!');
				window.location.href = 'bookInformationMaintenance.php';
				</script>";

			}

		}
	}
	
	$bookId = "";
	if (isset($_GET['bookId'])) {
		$bookId = $_GET['bookId'];
	}
	
	if ($bookId != null && $bookId != "") {
		$result = mysqli_query($mysqli, "SELECT * FROM book_information WHERE book_id = '".$bookId."'");
		$row = mysqli_fetch_array($result);
	}
?>

<script type="text/javascript">
$(document).ready(function() {
	$('#bookDateFinished').datepicker({
		format: 'yyyy-mm-dd'
	});
	
	$('.datepicker.dropdown-menu').click(function() {
		$(this).hide();
	});

	$( "#sortable" ).sortable({
		start: function(e, ui) {
	        // creates a temporary attribute on the element with the old index
	        $(this).attr('data-previndex', ui.item.index());
	    },
		update: function(e, ui) {
			// gets the new and old index then removes the temporary attribute
	        var newIndex = ui.item.index();
        	var oldIndex = $(this).attr('data-previndex');

        	console.log(newIndex);
        	console.log(oldIndex);

        	$.ajax({
			  type: "GET",
			  url: "sort.php",
			  data: { 
			  	"id": ui.item[0].id,
			  	"newIndex": newIndex,
			  	"oldIndex": oldIndex
			  }
			});
	        
			$(this).removeAttr('data-previndex');
	    }
	});
    $( "#sortable" ).disableSelection();

    if (jQuery("#bSchool").val() != "0") {
    	initDepartment();
    	setTimeout(function() {
    		initCourse();
		}, 1000);

    }

    jQuery("#bSchool").on("change", function() {
    	initDepartment();
    });

    function initDepartment() {
    	$.ajax({
		  type: "GET",
		  url: "departmentQuery.php",
		  data: { 
		  	"schoolId": $("#bSchool option:selected").attr("id"),
		  	"bookId": $("#bookId").val()
		  },
		  success: function(data) {
		  	var jsonData = JSON.parse(data);
		  	var content = "<option id='0'>Select Department</option>";
		  	for (var i=0; i<jsonData.length; i++) {
		  		var isSelected = jsonData[i].isSelected === true ? "selected" : "";
		  		content += "<option id="+jsonData[i].departmentId+" value='"+jsonData[i].departmentName+"' "+isSelected+">" + jsonData[i].departmentName + "</option>";
		  	}
		  	jQuery("#bDepartment").html(content);
		  }
		});
    }

    jQuery("#bDepartment").on("change", function() {
    	initCourse();
    });

    function initCourse() {
    	$.ajax({
		  type: "GET",
		  url: "courseQuery.php",
		  data: { 
		  	"departmentId": $("#bDepartment option:selected").attr("id"),
		  	"bookId": $("#bookId").val()
		  },
		  success: function(data) {
		  	var jsonData = JSON.parse(data);
		  	var content = "<option id='0'>Select Course</option>";
		  	for (var i=0; i<jsonData.length; i++) {
		  		var isSelected = jsonData[i].isSelected === true ? "selected" : "";
		  		content += "<option id="+jsonData[i].courseId+" value='"+jsonData[i].courseName+"' "+isSelected+">" + jsonData[i].courseName + "</option>";
		  	}
		  	jQuery("#bCourse").html(content);
		  }
		});
    }

});
</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<?php if(isset($row['book_id'])) { ?>
<input type="hidden" id="bookId" name="bookId" value="<?php echo $row['book_id'] ?>" />
<?php } else { ?>
<input type="hidden" id="bookId" name="bookId" value="0" />
<?php } ?>
<h3>
    <span class="label label-primary">Book Information Maintenance</span>
</h3>
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0">

      <tr>
        <td class="col-md-2">
            <label class="form-control-static">Book School</label>
        </td>
        <?php 
        if ($bookId != null && $bookId != "") {
		?>
	        <td class="col-md-2">
	            <p class="form-control-static">
	                <select name="bSchool" id="bSchool">
	                <option id="0">Select School</option>
					<?php

	                	$bs_query = $mysqli->query("SELECT * FROM book_school");
			    		while($bs_row = $bs_query->fetch_assoc()) {

						$newQuery = $mysqli->query("SELECT book_school FROM book_information WHERE book_id = '".$bookId."'");
						$nQuery = $newQuery->fetch_assoc();

						$isSelected = "";
						if ($nQuery['book_school'] === $bs_row['school_name']) {
							$isSelected = "selected";
						}

		    		?>
				  	<option id="<?php echo $bs_row['school_id']; ?>" value="<?php echo $bs_row['school_name']; ?>" <?php echo $isSelected ?>><?php echo $bs_row['school_name']; ?></option>
				  	<?php
	                	}
		    		?>
					</select>
	            </p>
	        </td>
	        <td class="col-md-2">
	            <label class="form-control-static">Book Department</label>
	        </td>
	        <td class="col-md-2">
	            <p class="form-control-static">
	                <select name="bDepartment" id="bDepartment">
					  <option value="0" selected="selected" disabled="disabled">Please select book department</option>
					</select>
	            </p>
	        </td>
	        <td class="col-md-2">
	            <label class="form-control-static">Book Course</label>
	        </td>
	        <td>
	            <p class="form-control-static">
	                <select name="bCourse" id="bCourse">
					  <option value="0" selected="selected" disabled="disabled">Please select book department</option>
					</select>
	            </p>
	        </td>
	    <?php
        } else {
        	$bs_query = "SELECT * FROM book_school";
			$bs_query = $mysqli->query($bs_query);
	        ?>
	        <td>
	            <p class="form-control-static">
	                <select name="bSchool" id="bSchool">
	                <option id="0" selected="selected">Select School</option>
					<?php
	                	// output data of each row
			    		while($bs_row = $bs_query->fetch_assoc()) {
		    		?>
				  	<option id="<?php echo $bs_row['school_id']; ?>" value="<?php echo $bs_row['school_name']; ?>"><?php echo $bs_row['school_name']; ?></option>
				  	<?php
	                	}
		    		?>
					</select>
	            </p>
	        </td>
	        <td>
	            <label class="form-control-static">Book Department</label>
	        </td>
	        <td>
	            <p class="form-control-static">
	                <select name="bDepartment" id="bDepartment">
					  <option value="0" selected="selected" disabled="disabled">Please select book department</option>
					</select>
	            </p>
	        </td>
	        <td>
	            <label class="form-control-static">Book Course</label>
	        </td>
	        <td>
	            <p class="form-control-static">
	                <select name="bCourse" id="bCourse">
					  <option value="0" selected="selected" disabled="disabled">Please select book department</option>
					</select>
	            </p>
	        </td>
	    <?php } ?>
	  </tr>

	  <tr>
		  
	  </tr>
    
	  <tr>

	  	<td class="col-md-2">
	        <label class="form-control-static">Book Title</label>
	    </td>
	    <td class="col-md-2">
	        <p class="form-control-static">
	            <input type="text" class="form-control" id="bookTitle" name="bookTitle" 
	            value="<?php if ($bookId != null && $bookId != "") { echo $row['book_title']; } ?>" required autofocus />
	        </p>
	    </td>

      	<td>
        	<label class="form-control-static">Book Date Accomplised</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="bookDateFinished" name="bookDateFinished" 
                value="<?php if ($bookId != null && $bookId != "") { echo $row['book_date_finished']; } ?>" required />
            </p>
        </td>
      </tr>
	
      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Book Author 1</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="bookAuthor1" name="bookAuthor1" 
                value="<?php if ($bookId != null && $bookId != "") { echo $row['book_author_1']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Book Author 2</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="bookAuthor2" name="bookAuthor2" 
                value="<?php if ($bookId != null && $bookId != "") { echo $row['book_author_2']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Book Author 3</label>
        </td>
		<td class="col-md-3">
            <p class="form-control-static">
                <input type="text" class="form-control" name="bookAuthor3" id="bookAuthor3" 
                value="<?php if ($bookId != null && $bookId != "") { echo $row['book_author_3']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Book Author 4</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="bookAuthor4" name="bookAuthor4" 
                value="<?php if ($bookId != null && $bookId != "") { echo $row['book_author_4']; } ?>" required />
            </p>
        </td>
      </tr>
	  
	  <tr>
      	<td colspan="6">
			<button type="submit" class="btn btn-primary pull-right" name="save" id="save">Submit</button>	
       </td>
     </tr>

     <ul id="sortable">
	     <?php
	     	if ($bookId != null && $bookId != "") {
				$result2 = mysqli_query($mysqli, "SELECT * FROM book_upload WHERE book_id = '".$bookId."' ORDER BY sort_id ASC");
				$counter = 1;
				while ($row2 = mysqli_fetch_array($result2)) {
	     ?>
		  <li class="ui-state-default" id="<?php echo $row2['id'] ?>">
		  	<div>
		  		Page <?php echo $counter++ ?><div class="pull-right"><a href="delete.php?bookId=<?php echo $_GET['bookId']; ?>&amp;id=<?php echo $row2['id']; ?>">X</a></div>
		  		<?php echo '<img src="data:image/jpeg;base64,'. base64_encode($row2['book_image']).'" width="150" height="300">'; ?>
		  	</div>
		  </li>
		<?php } } ?>
	</ul>
	  
	<tr>
      	<td colspan="6">
			<a href="../fileupload/fileUpload.php?bookId=<?php echo $row['book_id']; ?>">
				<button type="button" class="btn btn-primary pull-right" name="changePhotos" id="changePhotos">Change Photos</button>
			</a>
	    </td>
      </tr>
	  
    </table>
    
</div>

</form>
</div>
<?php 
if ($bookId != null && $bookId != "") { 
	"}";
}
 ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>