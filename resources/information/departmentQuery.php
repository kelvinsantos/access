<?php

require_once(realpath(dirname(__FILE__) . "/../config.php"));

$sql = "SELECT * FROM book_department WHERE school_id = '".$_GET['schoolId']."'";
$result = $mysqli->query($sql);
			
if ($result->num_rows > 0) {
    
    $arrList = array();
    while($row = $result->fetch_assoc()) {
    	$sql2 = "SELECT * FROM book_information WHERE book_id = '".$_GET['bookId']."'";
		$result2 = $mysqli->query($sql2);
		while($row2 = $result2->fetch_assoc()) {
			$isSelected = false;
			if ($row2['book_department'] === $row['department_name']) {
				$isSelected = true;
			}
		}

        $arr = array('departmentId' => $row['department_id'], 'departmentName' => $row['department_name'], 'isSelected' => $isSelected);
        array_push($arrList, $arr);
    }
    echo json_encode($arrList);
} else {
    echo "0 results";
}
$mysqli->close();

?>