<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	$bootstrapdatepickercss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$bootstrapdatepicker = "1";

	$title = "Administrator List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	if (isset($_POST['save'])) {
		$accountId = $_POST['accountId'];
		if ($accountId != null && $accountId != "0") {
			
			$sql="UPDATE accounts SET email = '".$_POST['accountEmail']."', password = '".$_POST['accountPassword']."' 
				WHERE account_id = '".$_POST['accountId']."'";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			$sql="UPDATE admin_information SET 
				id_number = '".$_POST['idNumber']."',
				full_name = '".$_POST['fullName']."',
				position = '".$_POST['position']."',
				date_of_birth = '".$_POST['dateOfBirth']."',
				phone = '".$_POST['phone']."'
				WHERE account_id = $accountId";
				
				if (!mysqli_query($mysqli,$sql)) {
					die('Error: ' . mysqli_error($mysqli));
				}
					
				mysqli_close($mysqli);
				
				echo "<script>
				alert('Update Success!');
				window.location.href = 'adminInformationList.php';
				</script>";

				break;
				
		} else {
			
			$sql="INSERT INTO accounts (email, password, role) VALUES ('".$_POST['accountEmail']."', '".$_POST['accountPassword']."', 'admin')";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			$accountId = $mysqli->insert_id;
			
			$sql="INSERT INTO admin_information (
				account_id, 
				id_number, 
				full_name, 
				position, 
				date_of_birth,
				home_address, 
				phone)
			VALUES
				(
				'".$accountId."', 
				'".$_POST['idNumber']."', 
				'".$_POST['fullName']."', 
				'".$_POST['position']."', 
				'".$_POST['dateOfBirth']."', 
				'".$_POST['homeAddress']."', 
				'".$_POST['phone']."')";
				
				if (!mysqli_query($mysqli,$sql)) {
					die('Error: ' . mysqli_error($mysqli));
				}
					
				mysqli_close($mysqli);
				
				echo "<script>
				alert('Insert Success!');
				window.location.href = 'adminInformationMaintenance.php?accountId=".$accountId."';
				</script>";

				break;
		}
	}
	
	$accountId = "";
	if (isset($_GET['accountId'])) {
		$accountId = $_GET['accountId'];
	}
	
	if ($accountId != null && $accountId != "") {	
		$result = mysqli_query($mysqli, "SELECT * FROM admin_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$accountId."'");
		$row = mysqli_fetch_array($result);
	}
?>

<script type="text/javascript">
$(document).ready(function() {
	$('#dateOfBirth').datepicker({
		format: 'yyyy-mm-dd'
	});
	
	$('.datepicker.dropdown-menu').click(function() {
		$(this).hide();
	});
});
</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<?php if(isset($row['account_id'])) { ?>
<input type="hidden" id="accountId" name="accountId" value="<?php echo $row['account_id'] ?>" />
<?php } else { ?>
<input type="hidden" id="accountId" name="accountId" value="0" />
<?php } ?>
<h3>
    <span class="label label-primary">Administrator Information Maintenance</span>
</h3>
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0">
	  
      <tr>
        <td class="col-md-2">
            <label class="form-control-static">Account Name</label>
        </td>
        <td class="col-md-2">
            <p class="form-control-static">
                <input type="text" class="form-control" id="accountEmail" name="accountEmail" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['email']; } ?>" required autofocus />
            </p>
        </td>
        <td>
            <label class="form-control-static">Account Password</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="password" class="form-control" id="accountPassword" name="accountPassword" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['password']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">ID Number</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="idNumber" name="idNumber" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['id_number']; } ?>" required />
            </p>
        </td>
      </tr>
    
      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Full Name of Admin</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fullName" name="fullName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['full_name']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Position</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="position" name="position" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['position']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
      	<td>
        	<label class="form-control-static">Date of Birth</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['date_of_birth']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Home Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" name="homeAddress" id="homeAddress" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['home_address']; } ?>" required />
            </p>
        </td>
      </tr>
   
      <tr>
        <td>
            <label class="form-control-static">Phone</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="phone" name="phone" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['phone']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
      	<td colspan="6">
        	<button type="submit" class="btn btn-primary pull-right" name="save" id="save">Submit</button>	
        </td>
      </tr>

    </table>
    
</div>

</form>
</div>
<?php 
if ($accountId != null && $accountId != "") { 
	"}";
}
 ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>