<?php

require_once(realpath(dirname(__FILE__) . "/../config.php"));

$sql = "SELECT * FROM book_course WHERE department_id = '".$_GET['departmentId']."'";
$result = $mysqli->query($sql);
			
if ($result->num_rows > 0) {
    
    $arrList = array();
    while($row = $result->fetch_assoc()) {
    	$sql2 = "SELECT * FROM book_information WHERE book_id = '".$_GET['bookId']."'";
		$result2 = $mysqli->query($sql2);
    	while($row2 = $result2->fetch_assoc()) {
    		$isSelected = false;
			if ($row2['book_course'] === $row['course_name']) {
				$isSelected = true;
			}
		}

        $arr = array('courseId' => $row['course_id'], 'courseName' => $row['course_name'], 'isSelected' => $isSelected);
        array_push($arrList, $arr);
    }
    echo json_encode($arrList);
} else {
    echo "0 results";
}
$mysqli->close();

?>