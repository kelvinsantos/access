<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Book Archive";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
    $result = mysqli_query($mysqli,"SELECT * FROM book_information WHERE book_id = '".$_GET['bookId']."'");
	$row = mysqli_fetch_array($result);
?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var printWindow = window.open();
        printWindow.document.write('<html>');
		printWindow.document.write('	<head>');
		printWindow.document.write('		<title>Book Information Sheet</title>');
		printWindow.document.write('		<link rel="stylesheet" href="../../public_html/css/bootstrap.min.css">');
		printWindow.document.write('	</head>');
        printWindow.document.write('<body>');
		printWindow.document.write('<br>');
        printWindow.document.write(data);
		printWindow.document.write(jQuery(".printFooter").html());
        printWindow.document.write('</body>');
		printWindow.document.write('</html>');
        printWindow.print();
        printWindow.close();
		window.location.reload();

        return true;
    }

</script>

<div class="container">
<form class="form-horizontal" role="form">
<h3><span class="label label-primary">Book Information</span></h3>
<br />
<button type="button" class="btn btn-primary" onclick="PrintElem('.table-responsive')">
  <span class="glyphicon glyphicon-print"></span> Print
</button>
<a href="../accounts/bookArchive.php">
<button type="button" class="btn btn-primary pull-right">
  <span class="glyphicon glyphicon-repeat"></span> Back to Book Archive
</button>
</a>
<br />
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0" border="1">
	
	  <tr>
        <td>
            <label class="form-control-static">Title</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['book_title'] ?></p>
        </td>
      </tr>
	  
	  <tr>
        <td class="col-md-3">
            <label class="form-control-static">Department </label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static"><?php echo $row['book_department'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Course</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['book_course'] ?></p>
        </td>
      </tr>
	  
      <tr>
        <td>
            <label class="form-control-static">Date Finished</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['book_date_finished'] ?></p>
        </td>
      </tr>
      
	  <tr>
        <td>
            <label class="form-control-static">Author 1</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['book_author_1'] ?></p>
        </td>
      </tr>
	  
	  <tr>
        <td>
            <label class="form-control-static">Author 2</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['book_author_2'] ?></p>
        </td>
      </tr>
	  
	  <tr>
        <td>
            <label class="form-control-static">Author 3</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['book_author_3'] ?></p>
        </td>
      </tr>
	  
	  <tr>
        <td>
            <label class="form-control-static">Author 4</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['book_author_4'] ?></p>
        </td>
      </tr>

<div class="printFooter" style="display:none">

    <table class="table" width="100%" cellpadding="0" cellspacing="0" align="center">
    	
        <tr>
        	<td>
            	<label class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;____________________</label>
            </td>
            <td>
            	<label class="text-center">____________________</label>
            </td>
        </tr>
        <tr>
        	<td>
            	<label class="text-center"> Moderator Signature Over Printed Name </label>
            </td>
            <td>
            	<label class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</label>
            </td>
        </tr>
        
    </table>
    
    <?php echo $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?><br/>

	A.C.C.E.S.S. &copy 2015
	
</div>

</form>
</div>
<?php mysqli_close($mysqli); ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>