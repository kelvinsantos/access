<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Book List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	if (isset($_GET['op'])) {
		if ($_GET['op'] == 'deleteBook') {
			
			
			$sql="UPDATE book_information SET is_deleted = '1' WHERE book_id = '".$_GET['bookId']."'";
		
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			mysqli_close($mysqli);
					
			echo "<script>
			alert('Delete Success!');
			window.location.href = 'bookInformationList.php';
			</script>";

			break;
		}
	}
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#addBook").click(function() {
		window.location.href = "bookInformationMaintenance.php";	
	});
});

</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<h3><span class="label label-primary"><?php echo $title ?></span></h3>
<br />
<div class="table-responsive">
	<table class="table table-striped">
  		<tr>
        	<th>#</th>
            <th>Book Title</th>
            <th>Department</th>
            <th>Course</th>
            <th>Date Accomplished</th>
            <th>Author 1</th>
            <th>Author 2</th>
            <th>Author 3</th>
            <th>Author 4</th>
            <th>Action</th>
        </tr>
        
        <?php 
		$result = mysqli_query($mysqli,"SELECT * FROM book_information WHERE is_deleted = '0'");
		while ($row = mysqli_fetch_array($result)) {
		?>
        
        <tr>
        	<td><?php echo $row['book_id']; ?></td>
            <td><?php echo $row['book_title']; ?></td>
            <td><?php echo $row['book_department']; ?></td>
            <td><?php echo $row['book_course']; ?></td>
            <td><?php echo $row['book_date_finished']; ?></td>
            <td><?php echo $row['book_author_1']; ?></td>
            <td><?php echo $row['book_author_2']; ?></td>
            <td><?php echo $row['book_author_3']; ?></td>
            <td><?php echo $row['book_author_4']; ?></td>
            <td><a href="bookInformationMaintenance.php?bookId=<?php echo $row['book_id']; ?>">Edit</a> | <a href="bookInformationList.php?op=deleteBook&bookId=<?php echo $row['book_id']; ?>">Delete</a></td>
        </tr>
        
        <?php } ?>
	</table>
    <button type="button" class="btn btn-primary pull-right" name="addBook" id="addBook">Add Book</button>
</div>
</form>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>