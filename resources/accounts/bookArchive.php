<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$jquerydatatablescss = "1";
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$jquerydatatablesjs = "1";
?>

<?php 
	$title = "Book Archive";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
?>

<script type="text/javascript">
	$(document).ready(function() {

	    // Setup - add a text input to each footer cell
	    $('#bookArchiveTable tfoot tr th').each(function() {
	        var title = $('#bookArchiveTable thead tr th').eq($(this).index()).text();
	        $(this).html('<input type="text" placeholder="Search '+title+'" />');
	    });
	 
	    // DataTable
	    var table = $('#bookArchiveTable').DataTable();
	 
	    // Apply the search
	    table.columns().eq(0).each(function(colIdx) {
	        $('input', table.column(colIdx).footer()).on('keyup change', function() {
	            table
	                .column(colIdx)
	                .search(this.value)
	                .draw();
	        });
	    });

		$('#bookArchiveTable #idx').on('click', function() {
			var bookId = $(this).attr('bookId');
			window.location.href = "/access/resources/information/bookInformation.php?bookId=" + bookId;
		});
	
	});
</script>

<?php
$result = mysqli_query($mysqli,"SELECT * FROM book_information WHERE is_deleted = '0'");
?>
<div class='container'>
<div class="row">
  <div class="col-md-11">
  	<h3><span class="label label-primary">Book Archive</span></h3>
  </div>
  <div class="col-md-1" style="line-height: 56px;">
  	<button type="button" class="btn btn-primary" onclick="printPage()">
	  <span class="glyphicon glyphicon-print"></span> Print
	</button>
  </div>
</div>
<br />
	<table cellpadding='0' cellspacing='0' border='0' class='display' id='bookArchiveTable'>
        <thead>
            <tr>
                <th>Title</th>
                <th>Department</th>
                <th>Course</th>
                <th>Date Finished</th>
                <th>Author 1</th>
                <th>Author 2</th>
                <th>Author 3</th>
                <th>Author 4</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>Title</th>
                <th>Department</th>
                <th>Course</th>
                <th>Date Finished</th>
                <th>Author 1</th>
                <th>Author 2</th>
                <th>Author 3</th>
                <th>Author 4</th>
            </tr>
        </tfoot>
    <tbody>

		<?php
		while($row = mysqli_fetch_array($result))
		  {
		  echo "<tr id='idx' bookId=".$row['book_id'].">";
		  echo "<td>".$row['book_title']."</td>";
		  echo "<td>".$row['book_department']."</td>";
		  echo "<td>".$row['book_course']."</td>";
		  echo "<td>".$row['book_date_finished']."</td>";
		  echo "<td>".$row['book_author_1']."</td>";
		  echo "<td>".$row['book_author_2']."</td>";
		  echo "<td>".$row['book_author_3']."</td>";
		  echo "<td>".$row['book_author_4']."</td>";
		  echo "</tr>";
		  }
		  
		  mysqli_close($mysqli);
		?>

	</tbody>
	</table>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>