<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo $title ?></title>
    
    <?php 
		require_once(PUBLIC_HTML_PATH . "/css/access.css");
		require_once(LIBRARY_PATH . "/extcss.php");
		
		// Change the line below to your timezone!
		date_default_timezone_set('Asia/Manila');
		$date = date('d-M-Y');
		
		if (isset($jqueryjs)) {
			$jqueryjs = $jqueryjs;
		} else {
			$jqueryjs = "";
		}
		
		if ($jqueryjs == "1") {
			echo '<script src="../../public_html/js/jquery-1.11.1.min.js"></script>';
		}
		
		$disabledTimeOut=false;
    $role = $_SESSION['role'];
	?>
    
    <?php if ($useUpdateClock) { ?>
    	<script type="text/javascript">
        setInterval('updateClock()', 1000);
      </script>
    <?php } ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>
    <div id="wrap">
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"> A.C.C.E.S.S </a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

            <?php if ($role == "admin") { ?>
			
              <li <?php echo ($title == 'Book List' ? "class='active'" : ""); ?>><a href="../information/bookInformationList.php">Book List</a></li>
              <li <?php echo ($title == 'Book Archive' ? "class='active'" : ""); ?>><a href="../accounts/bookArchive.php">Book Archive</a></li>
			  
            <?php } ?>

            <?php if ($role == "owner") { ?>

              <li <?php echo ($title == 'Book List' ? "class='active'" : ""); ?>><a href="../information/bookInformationList.php">Book List</a></li>
              <li <?php echo ($title == 'Book Archive' ? "class='active'" : ""); ?>><a href="../accounts/bookArchive.php">Book Archive</a></li>
              <li <?php echo ($title == 'Administrator List' ? "class='active'" : ""); ?>><a href="../information/adminInformationList.php">Administrator List</a></li>

            <?php } ?>
	
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['fullName']; ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="../login/logout.php">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>