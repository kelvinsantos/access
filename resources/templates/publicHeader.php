<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo $title ?></title>
    
    <?php 
		require_once(PUBLIC_HTML_PATH . "/css/access.css");
		require_once(LIBRARY_PATH . "/extcss.php");
		
		// Change the line below to your timezone!
		date_default_timezone_set('Asia/Manila');
		$date = date('d-M-Y');
		
		if (isset($jqueryjs)) {
			$jqueryjs = $jqueryjs;
		} else {
			$jqueryjs = "";
		}
		
		if ($jqueryjs == "1") {
			echo '<script src="../../public_html/js/jquery-1.11.1.min.js"></script>';
		}
		
		$disabledTimeOut=false;
	?>
    
    <?php if ($useUpdateClock) { ?>
    	<script type="text/javascript">
        setInterval('updateClock()', 1000);
      </script>
    <?php } ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>
    <div id="wrap">
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="../../home/"> A.C.C.E.S.S </a>
          </div>
        </div>
      </div>