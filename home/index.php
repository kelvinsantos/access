<?php 
session_start();
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

			<title> A.C.C.E.S.S - A Compilation Comprising the Entire Study of Students </title>

				<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">

				<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
				<link href="css/animate.css" rel="stylesheet" />

				<link href="css/style.css" rel="stylesheet">
				<link href="color/default.css" rel="stylesheet">
				<link rel="stylesheet" href="css/custombox.min.css">
				
					<script language="JavaScript1.2">
 
						function disableselect(e){
							return false
						}
 
						function reEnable(){
							return true
						}
 
						document.onselectstart=new Function ("return false")
 
						if (window.sidebar){
							document.onmousedown=disableselect
							document.onclick=reEnable
						}
						
					</script>
					<script type="text/javascript">
						$("#query").keyup(function(event){
							if(event.keyCode == 13){
								$("#showResults").click();
							}
						});
					</script>
	</head>

		<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

			<div id="preloader">
				<div id="load"></div>
			</div>

				<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
							<i class="fa fa-bars"></i>
							</button>
							<a class="navbar-brand" href="#intro">
							<h1> A.C.C.E.S.S </h1>
							</a>
						</div>
						<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#intro"> Home </a></li>
								<li><a href="#about"> About </a></li>
								<li><a href="#developers"> Developers </a></li>
							</ul>
						</div>
					</div>
				</nav>

				<section id="intro" class="intro">
	
					<div class="slogan">
						<h2> This is <span class="text_color"> A.C.C.E.S.S </span> </h2>
						<h4> A Compilation Comprising the Entire Study of Students  </h4>
					</div>
					
					<div class="slogan">
							<form action="../resources/public_accounts/publicBookArchive.php" method="post">
								<input type="search" name="query" id="query" placeholder=" Search A.C.C.E.S.S " autofocus>
								<button type="submit" name="showResults" id="showResults"> <img src="img/icons/book_search.png"></img> </button>
								<h4> Note: You may search for the Title, School, Department, Course or the Year the Study was created.  </h4>
							</form>
					</div>
					
				</section>
					
				<section id="about" class="home-section text-center bg-gray about">	
					<div class="heading-about">
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="wow bounceInDown" data-wow-delay="0.4s">
										<div class="section-heading">
											<h2> About A.C.C.E.S.S </h2>
											<i class="fa fa-2x fa-angle-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-sm-3 col-md-3">
								<div class="wow fadeInLeft" data-wow-delay="0.2s">
									<div class="service-box">
										<div class="service-desc">
											<h5> A.C.C.E.S.S </h5>
											<p> provides accurate information to students. </p>
										</div>
										<div class="service-icon">
											<img src="img/description/icon1.png" alt="" />
										</div>
									</div>
								</div>
							</div>
							<br/><br/><br/>
							<div class="col-sm-3 col-md-3">
								<div class="wow fadeInLeft" data-wow-delay="0.2s">
									<div class="service-box">
										<div class="service-icon">
											<img src="img/description/icon2.png" alt="" />
										</div>
										<div class="service-desc">
											<h5> Also.. </h5>
											<p> It gives students the exact data that the students wants to know. </p>
										</div>
									</div>
								</div>
							</div>
							<br/><br/><br/>
							<div class="col-sm-3 col-md-3">
								<div class="wow fadeInLeft" data-wow-delay="0.2s">
									<div class="service-box">
										<div class="service-desc">
											<h5> And.. </h5>
											<p> Because of its responsive design, A.C.E.S.S. is very easy to navigate. </p>
										</div>
										<div class="service-icon">
											<img src="img/description/icon3.png" alt="" />
										</div>
									</div>
								</div>
							</div>
							<br/><br/><br/>
							<div class="col-sm-3 col-md-3">
								<div class="wow fadeInLeft" data-wow-delay="0.2s">
									<div class="service-box">
										<div class="service-icon">
											<img src="img/description/icon4.png" alt="" />
										</div>
										<div class="service-desc">
											<h5> So.. </h5>
											<p> students will have less time searching for previous research titles. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br/>
					</div>
				</section>
					
					
				<section id="developers" class="home-section text-center developers">
					<div class="heading-about">
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="wow bounceInDown" data-wow-delay="0.4s">
										<div class="section-heading">
											<h2>About us</h2>
											<i class="fa fa-2x fa-angle-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-6 col-sm-3 col-md-3">
								<div class="wow bounceInUp" data-wow-delay="0.2s">
									<div class="team boxed-grey">
										<div class="inner">
											<h5> John Paul S. Pangan </h5>
											<p class="subtitle">  </p>
											<div class="avatar"><img src="img/developer/default.jpg" alt="" class="img-responsive img-circle" /></div>
										</div>
									</div>
								</div>
							</div>
							<br/><br/>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<div class="wow bounceInUp" data-wow-delay="0.5s">
									<div class="team boxed-grey">
										<div class="inner">
											<h5> Jayvee S. Salac </h5>
											<p class="subtitle"> </p>
											<div class="avatar"><img src="img/developer/jayveesalac.jpg" alt="" class="img-responsive img-circle" /></div>
										</div>
									</div>
								</div>
							</div>
							<br/><br/>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<div class="wow bounceInUp" data-wow-delay="0.8s">
									<div class="team boxed-grey">
										<div class="inner">
											<h5> Nickie Jane C. Salangsang </h5>
											<p class="subtitle"> </p>
											<div class="avatar"><img src="img/developer/default.jpg" alt="" class="img-responsive img-circle" /></div>
										</div>
									</div>
								</div>
							</div>
							<br/><br/><br/>
							<div class="col-xs-6 col-sm-3 col-md-3">
								<div class="wow bounceInUp" data-wow-delay="1s">
									<div class="team boxed-grey">
										<div class="inner">
											<h5> Edlyn P. Sanchez </h5>
											<p class="subtitle"> </p>
											<div class="avatar"><img src="img/developer/default.jpg" alt="" class="img-responsive img-circle" /></div>
										</div>
									</div>
								</div>
							</div>
						</div>		
					</div>
				</section>
				
				<footer>
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="wow shake" data-wow-delay="0.4s">
									<div class="page-scroll marginbot-30">
										<a href="#intro" id="totop" class="btn btn-circle">
											<i class="fa fa-angle-double-up animated"></i>
										</a>
									</div>
								</div>
									<a href="../resources/login"> <img src="img/icons/access_home.png"> </img></a>
									<p> Copyright &copy; 2015. </p>
							</div>
						</div>	
					</div>
				</footer>

			<script src="js/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/jquery.easing.min.js"></script>	
			<script src="js/jquery.scrollTo.js"></script>
			<script src="js/wow.min.js"></script>

			<script src="js/custom.js"></script>
			<script src="js/custombox.min.js"></script>
		</body>

</html>
