<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
	
<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "A.C.C.E.S.S.";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../resources/config.php"));
	require_once(TEMPLATES_PATH . "/publicHeader.php");
	
	$sql = "SELECT * FROM book_information a LEFT JOIN book_upload b ON b.book_id = a.book_id WHERE a.book_id = '".$_GET['bookId']."'";
	$sql = $mysqli->query($sql);
?>

    <link href="booklet/jquery.booklet.latest.css" type="text/css" rel="stylesheet" media="screen, projection, tv" />
    <style type="text/css">
        body {background:#ccc; font:normal 12px/1.2 arial, verdana, sans-serif;}
    </style>
	
	<script language="JavaScript1.2">
 
		function disableselect(e){
			return false
		}
 
		function reEnable(){
			return true
		}
 
		document.onselectstart=new Function ("return false")

			if (window.sidebar){
				document.onmousedown=disableselect
				document.onclick=reEnable
			}
						
	</script>
</head>
<body>
			
	<header>
		<h1> <div>
			<a href="javascript:close_window();"> <button> Close </button></a>
		</div></h1>
	</header>
	<section>
	    <div id="mybook">
	    	<?php while($row = $sql->fetch_assoc()) { ?>
	        <div title="<?php echo $row['id']; ?>">
	            <?php echo '<img src="data:image/jpeg;base64,'. base64_encode($row['book_image']).'">'; ?>
	        </div>
	        <?php } ?>
	        <div title="nineth page">
	            <h3> A.C.C.E.S.S. &amp;copy 2015 </h3>
	        </div>
	    </div>
	</section>
	
	<!--?php echo '<img src="data:image/jpeg;base64,'.base64_encode($image3).'" width="450" height="450"/>'; ?>-->
	
	<footer></footer>
    <script src="booklet/jquery-2.1.0.min.js"></script>
    <script src="booklet/jquery-ui-1.10.4.min.js"></script>
    <script src="booklet/jquery.easing.1.3.js"></script>
    <script src="booklet/jquery.booklet.latest.js"></script>
	<script>
	    $(function () {		
	        $("#mybook").booklet();
	    });
		
		function close_window() {
			if (confirm("Close Window?")) {
		close();
		}
	}
    </script>
	<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>
</body>
</html>